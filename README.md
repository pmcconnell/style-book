#  Style Book

A Mac app to display the built in font styles available in SwiftUI

![StyleBook Screenshot](/StyleBook_Screen.png "Screenshot")

I may or may not add more as I find a need. I'm constantly forgetting many things that might fit here but I can't recall them now...

This code is provided free of use. Feel free to use the app or code in any way. If you add anything interesting I'd love to hear about it.

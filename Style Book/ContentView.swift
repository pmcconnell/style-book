//
//  ContentView.swift
//  Style Book
//
//  Created by Patrick McConnell on 4/7/24.
//

import SwiftUI

struct FontStyle: Identifiable {
    var id: UUID = UUID()
    var name: String
    var style: Font

    static var all: [FontStyle] {
        return [
            FontStyle(name: "Footnote", style: .footnote),
            FontStyle(name: "Caption2", style: .caption2),
            FontStyle(name: "Caption", style: .caption),
            FontStyle(name: "Callout", style: .callout),
            FontStyle(name: "Body", style: .body),
            FontStyle(name: "Subheadline", style: .subheadline),
            FontStyle(name: "Headline", style: .headline),
            FontStyle(name: "Title3", style: .title3),
            FontStyle(name: "Title2", style: .title2),
            FontStyle(name: "Title", style: .title),
            FontStyle(name: "Large Title", style: .largeTitle)
        ]
    }
}

struct ContentView: View {

    @AppStorage("TextSample") private var textSample: String = "Hello, world!"
    @State private var fontColor: Color = .black

    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                TextField("Sample Text", text: $textSample)
                    .frame(maxWidth: 220)
                ColorPicker("Font Color", selection: $fontColor)
            }
            ForEach(FontStyle.all) {style in
                HStack {
                    Text(style.name)
                        .font(.caption)
                        .foregroundStyle(.gray)
                        .frame(width: 62, alignment: .leading)
                    Text(self.textSample)
                        .font(style.style)
                        .foregroundStyle(fontColor)
                }
            }
        }
        .padding()
        .fixedSize()
    }
}

#Preview {
    ContentView()
}

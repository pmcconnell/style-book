//
//  Style_BookApp.swift
//  Style Book
//
//  Created by Patrick McConnell on 4/7/24.
//

import SwiftUI

@main
struct Style_BookApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
        .windowResizability(.contentSize)
    }
}
